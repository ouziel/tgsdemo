var express = require('express')
var config = express()

function requireUncached(module){
    delete require.cache[require.resolve(module)]
    return require(module)
}

config.get('/', function(req,res) {
  var data = requireUncached('./data.json')
  res.send(data)
});


module.exports = config