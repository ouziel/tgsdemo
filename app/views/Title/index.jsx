import React, {PropTypes as T} from 'react'


export default class Title extends React.Component {

  static propTypes = {
    text: T.string,
    linkHref: T.string,
    linkText: T.string,
    color: T.string,
    fontSize: T.number
  }

  static defaultProps = {
    text: 'text with link',
    linkHref: '#',
    color: 'red',
    fontSize: 50,
    linkText: 'link',
  }

splitStringAtWord(text,word) {
      const index = text.search(word)
      const strRight = text.slice(index + word.length,text.length)
      const strLeft = text.slice(0,index)
      const str = `${strLeft} ${strRight}`
      return {strLeft,strRight}
  }

  render() {

    const {linkHref, linkText, color, fontSize,text} = this.props
    const {strLeft,strRight} = this.splitStringAtWord(text,linkText)

    return (
        <h1 style={{color, fontSize}}>
          {strLeft}
          <br/>
          <a href={linkHref}>{linkText}</a>
          {strRight}
        </h1>
    );
  }
}
