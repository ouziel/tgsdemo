import React, {PropTypes as T} from 'react'
import Layout from './Layout'
import Title from './Title'
import Subtitle from './Subtitle'
import Tiles from './Tiles'


export default class Page extends React.Component {

  static propTypes = {
    data: T.object.isRequired
  }

  render() {
    const {data} = this.props

    return (
      <Layout {...data.page} >
        <header className={'header'}>
          <Title {...data.title}/>
          <Subtitle {...data.subtitle}/>
        </header>
        <Tiles {...data.tiles}/>
      </Layout>
    );
  }
}

