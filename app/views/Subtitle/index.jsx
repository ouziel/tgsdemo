import React, {PropTypes as T} from 'react'


export default class Subtitle extends React.Component {

  static propTypes = {
    text: T.string,
  }

  static defaultProps = {
    text: "and i'm just a plain configureable text, still FOO tho"
  }
  render() {

    return (
        <p>{this.props.text}</p>
    );
  }
}

