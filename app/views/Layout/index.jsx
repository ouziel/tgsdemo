import React from 'react'
export default  class Layout extends React.Component {
  render() {
    const {children,title} = this.props
    return (
      <html>
        <head>
          <title>{title}</title>
          <link rel="stylesheet" href="./css/style.css"/>
        </head>
        <body>
          <div id="wrapper">
                <div id="main">
                  <div className="inner">
                      {children}
                </div>
              </div>
        </div>
        </body>
      </html>
    );
  }
}

