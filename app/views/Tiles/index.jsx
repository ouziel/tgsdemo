import React, {PropTypes as T} from 'react'
import classnames from 'classnames'
import Item from './item'

export default class Tiles extends React.Component {

  static propTypes = {
    data: T.array,
    reverse: T.bool,
    column: T.bool,
  }

  static defaultProps = {
    data: [],
    column: false,
    reverse: true,
    center: true,
  }

  render() {

    const {reverse, column, center, data} = this.props
    const sectionClass = classnames({
      tiles: true,
      column,
      reverse,
      center
    })

    return (
      <section className={sectionClass}>
        {data.map((item,index) =>  <Item key={index} index={index+1} {...item} />)}
      </section>
    );
  }
}
