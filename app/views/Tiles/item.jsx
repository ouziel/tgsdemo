import React, {PropTypes as T} from 'react'
export default class Item extends React.Component {

  static propTypes = {
    img: T.string,
    link: T.string,
    caption: T.string,
    index: T.number
  }

  static defaultProps = {
    img:'',
    link: 'www.foo.com',
    caption: 'some caption text',
    index:  0
  }

  render() {
    const {img, link, caption, index} = this.props
    return (
      <article className={`style${index}`}>
        <span className="image">
          <img src={img}/>
        </span>
        <a href={link} target="_blank">
          <h2>{caption}</h2>
        </a>
      </article>
    );
  }
}