const PORT = 80
const CONFIG_URL = 'http://localhost:3000/config'
const data = require('./app/config')
const express = require('express');
const axios = require('axios')
const sassMiddleware = require('node-sass-middleware');
const path = require('path')
const app = express();


app.set('views', path.join(__dirname, '/app/views'));
app.set('view engine', 'jsx');
app.engine('jsx', require('express-react-views').createEngine({
  babel:{
    "presets": ["es2015", "react", "stage-2"],
    "plugins": ["transform-object-rest-spread"]
  }
}));

app.use(sassMiddleware({
    src: path.join(__dirname, 'app/sass'),
    dest: path.join(__dirname, 'app/public/css'),
    debug: true,
    outputStyle: 'compressed',
    prefix:  '/css'
}));
app.use(express.static(path.join(__dirname, 'app/public')));
app.use('/config', data)
app.get('/',  (req, response) => {

  axios.get(CONFIG_URL)
    .then(({data}) => {
      response.render(
          'page',
           {data})
    })
    .catch(err => {
      res.send('no data')
    })

})

app.listen(PORT,  () => {
    console.log(`Listening on port ${PORT}`)
})